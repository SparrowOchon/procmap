package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/akamensky/argparse"
	"github.com/fatih/color"
	. "gitlab.com/SparrowOchon/procmap/src/lib/util"
)

/**
 * @Description: User passed parameter
 */
type UserInput struct {
	line_storage_buffer *int
	output_path         *string
	start_map           *bool
}

/**
 * @Description: Read data piped to process
 * @param pipe_reader The Stdin to read from
 * @param piped_message Channel to write Each Line to as (PipedMessage Object)
 */
func start_pipe_reader(pipe_reader *bufio.Reader, piped_message chan<- PipedMessage) {
	// Read Piped Info into a Buffered Channel
	go func() {
		read_pipe(pipe_reader, piped_message)
		close(piped_message)
	}()
}

/**
 * @Description: Threaded Pipe Reader. Reading the Program Stdin off the Pipe
 * @param reader The Stdin to read from
 * @param piped_line_channel Channel to write Each Line to as (PipedMessage Object)
 */
func read_pipe(reader *bufio.Reader, piped_line_channel chan<- PipedMessage) {

	/**
	Non-terminating
	**/
	for {
		line_value, err := reader.ReadBytes('\n')
		if err != nil && err == io.EOF {
			break
		}
		piped_line_channel <- line_value
	}
}

/**
 * @Description: Make a new txt temp file. In the PWD under a sub-directory /output/
 * @return string  The full path to the new file made
 */
func make_tmp_file() string {
	const default_output_dir = "/output"
	dir_name, err := os.Getwd()
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	dir_name += default_output_dir
	file, err := ioutil.TempFile(dir_name, "process_log.*.txt")
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	return file.Name()
}

/**
 * @Description: Validate user passed in parameters
 * @param info Input buffer from CLI
 * @return *UserInput Struct of user passed data
 */
func validate_input_parameters(info os.FileInfo) *UserInput {
	const DEFAULT_LINE_BUFFER_SIZE = 100
	var line_storage_buffer *int

	cli_parser := argparse.NewParser("procmap",
		"Map PIPE'd IP addresses accurately on a map." +
			"\n Examples: " +
		"     netstat -b | procmap -s <SAVE PATH>\n" +
		"               cat file.log | procmap \n" +
		"               echo <ip address> | procmap")
	line_storage_buffer = cli_parser.Int("b", "maxsize",
		&argparse.Options{Required: false,
			Help: "Size of preallocated Write Buffer. Larger = " +
				"Faster Writes (Default: 100)"})
	var output_location = cli_parser.String("o", "output",
		&argparse.Options{Required: false,
			Help: "Output Location (Defaults to PWD)"})
	var start_map = cli_parser.Flag("m", "map",
		&argparse.Options{Required: false,
			Help: "Open live map of parsed IPs."})
	err := cli_parser.Parse(os.Args)
	if err != nil || info.Mode()&os.ModeCharDevice != 0 {
		log.Fatal(cli_parser.Usage(err))
	}
	// if the dir doesnt exist or not provided we make a temp file.
	if !is_valid_path(*output_location){
		*output_location = make_tmp_file()
		fmt.Println("Invalid output path. Output will be stored at" +
			*output_location)
	}
	if *line_storage_buffer <= 0 {
		*line_storage_buffer = DEFAULT_LINE_BUFFER_SIZE // Make sure its always Positive
	}
	return &UserInput{line_storage_buffer: line_storage_buffer,
		output_path: output_location, start_map: start_map}
}

/**
 * @Description: Identify if file exists
 * @param path of file to validate
 * @return bool status of file existence
 */
func is_valid_path(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}