package resources

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"runtime"

	"github.com/fatih/color"
	"github.com/zserge/lorca"
	"gitlab.com/SparrowOchon/procmap/src/lib/util"
)
// Go types that are bound to the UI must be thread-safe, because each binding
// is executed in its own goroutine. In this simple case we may use atomic
// operations, but for more complex cases one should use proper synchronization.


func Draw(draw_storage chan util.WriterStorage) {
	args := []string{}
	if runtime.GOOS == "linux" {
		args = append(args, "--class=Lorca")
	}
	ui, err := lorca.New("", "", 800, 800, args...)
	defer ui.Close()
	if err != nil {
		log.Fatal(err)
	}

	// A simple way to know when UI is ready (uses body.onload event in JS)
	err = ui.Bind("read_ip_list", func() {
		go func() {
			for {
				line, alive := <-draw_storage
				if !alive {
					return
				}
				ui.Eval(fmt.Sprintf(`addCapture("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s",%f,%f,"%s");`,
					line.Country, line.City, line.Zip, line.Isp, line.Organization, line.AssignedOrganization,
					util.Bool_to_str(line.Proxy), util.Bool_to_str(line.Mobile), util.Bool_to_str(line.Hosting),
					line.Latitude, line.Longitude, line.Ip))

			}
		}()

	})
	if err != nil {
		color.Red(err.Error())
	}

	// Load HTML.
	// You may also use `data:text/html,<base64>` approach to load initial HTML,
	// e.g: ui.Load("data:text/html," + url.PathEscape(html))

	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()
	go http.Serve(ln, http.FileServer(FS))
	ui.Load(fmt.Sprintf("http://%s", ln.Addr()))

	// Wait until the interrupt signal arrives or browser window is closed
	<-ui.Done()


}
