package thread

import (
	"encoding/json"
	"github.com/fatih/color"
	. "gitlab.com/SparrowOchon/procmap/src/lib/util"
	"io/ioutil"
	"net/http"
	"regexp"
)

// No need to do for each call from the thread pool I.e set once on initialization
const ip_url = "http://ip-api.com/json/"
var ip_regex = regexp.MustCompile(`(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)`)

/**
 * @Description: Turn IP into geolocation
 * @param i (ProcessData) that is passed by the master thread to each thread in the pool
 */
func Ip_Info(i interface{}) {
	process_syscall := i.(ProcessData)
	var new_share SharedLine
	dirty_line := process_syscall.Line

	parsed_ip := ip_regex.FindString(string(dirty_line))
	if parsed_ip != "" {
		res, err := http.Get(ip_url + parsed_ip + "?fields=21702393")
		if err != nil {
			color.Red(err.Error())
			return
		}
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			color.Red(err.Error())
			return
		}
		json.Unmarshal(body, &new_share)
		process_syscall.Shared_Line <- new_share
	}
}