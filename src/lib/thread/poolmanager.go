package thread

import (
	"github.com/panjf2000/ants"
	. "gitlab.com/SparrowOchon/procmap/src/lib/util"
	"runtime"
	"sync"
)

/**
 * @Description: Initialize thread pool and launch it.
 * @param piped_lines Channel of read in lines from the Pipe
 * @param writer_storage channel of output lines to print to
 * @return *ants.PoolWithFunc Initialized thread pool
 * @return *sync.WaitGroup Wait group for the thread pool
 */
func Make_pool(piped_lines <-chan PipedMessage,
	writer_storage chan WriterStorage) (*ants.PoolWithFunc, *sync.WaitGroup) {

	thread_pool, pool_wait_group := build_thread_pool()
	go launch_threads(piped_lines, writer_storage, thread_pool, pool_wait_group)
	return thread_pool, pool_wait_group
}

/**
 * @Description: Build a thread pool
 * @return *ants.PoolWithFunc Initialized pool creation object
 * @return *sync.WaitGroup
 */
func build_thread_pool() (*ants.PoolWithFunc, *sync.WaitGroup) {
	const CPU_MULTIPLIER = 4 // Multiplier for thread count of pool.

	thread_count := runtime.NumCPU() * CPU_MULTIPLIER
	var pool_wait_group sync.WaitGroup
	thread_pool, _ := ants.NewPoolWithFunc(thread_count, func(i interface{}) {
		Ip_Info(i)
		pool_wait_group.Done()
	})
	return thread_pool, &pool_wait_group
}

/**
 * @Description: Launch work on a thread from the pool
 * @param piped_message Channel of messages read from the pipe
 * @param writer_storage Channel of Data returned from the Thread
 * @param thread_pool Initialized thread pool
 * @param pool_wait_group Wait group of thread pool. Keep track of whats running
 */
func launch_threads(piped_message <-chan PipedMessage, writer_storage chan WriterStorage,
	thread_pool *ants.PoolWithFunc, pool_wait_group *sync.WaitGroup) {

	/**
	Non-terminating
	**/
	for {
		line, alive := <-piped_message
		if !alive {
			break
		}
		pool_wait_group.Add(1)
		_ = thread_pool.Invoke(ProcessData{
			Line:                 line,
			Shared_Line:          writer_storage})
	}
	pool_wait_group.Wait()
	var termination_line SharedLine
	termination_line.Ip = LAST_MESSAGE_IDENTIFIER
	writer_storage <- termination_line
	close(writer_storage)

}
