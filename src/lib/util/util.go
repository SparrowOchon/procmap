package util

import (
	"strconv"
)

/**
 * @Description:  User defined buffer value. Each chunk added to the Writer_Storage will be of this value.
 */
const LAST_MESSAGE_IDENTIFIER = "~~!~~!~~!~~!_DONE_THREAD_!~~!~~!~~!~~"

/**
 * @Description:  Assignment type for the buffered channel with user passable buffer size
 */
type WriterStorage = SharedLine

/**
 * @Description: The message read form the user Pipe.
 */
type PipedMessage = []byte

/**
 * @Description:  Data returned by the thread
 */
type SharedLine struct {
	Country      string  `json:"country"`
	RegionName   string  `json:"regionName"`
	City         string  `json:"city"`
	District     string  `json:"district"`
	Zip          string  `json:"zip"`
	Latitude     float64  `json:"lat"`
	Longitude    float64  `json:"lon"`
	Isp          string  `json:"isp"`
	Organization   string  `json:"org"`
	AssignedOrganization string  `json:"asname"`
	Mobile          bool  `json:"mobile"`
	Proxy           bool  `json:"proxy"`
	Hosting         bool  `json:"hosting"`
	Ip              string  `json:"query"`
}

/**
 * @Description: Data shared to each thread in the thread pool
 */
type ProcessData struct {
	Line                 PipedMessage // Singular line of data from the input pipe
	Shared_Line          chan <-SharedLine // The Shared Data between thread and Manager
}

/**
 * @Description: Convert float to string
 * @param input_num float to convert to string
 * @return string string version of the float (Written numerically)
 */
func Float_to_str(input_num float64) string {
	return strconv.FormatFloat(input_num, 'f', 5, 64)
}

/**
 * @Description: Convert bool to string
 * @param input_bool boolean to convert to string
 * @return string string representation of the bool (true/false)
 */
func Bool_to_str(input_bool bool) string {
	return strconv.FormatBool(input_bool)
}