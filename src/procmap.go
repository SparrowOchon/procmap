//go:generate go run -tags generate uigenerate.go

package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/fatih/color"
	. "gitlab.com/SparrowOchon/procmap/src/lib/util"
	"gitlab.com/SparrowOchon/procmap/src/resources"
	"gitlab.com/SparrowOchon/procmap/src/lib/thread"
)

func main() {
	info, err := os.Stdin.Stat()
	if err != nil || info == nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	reader := bufio.NewReader(os.Stdin) // Linux has a 4kiB buffer for Pipes
	user_input := validate_input_parameters(info)

	// Space preallocated to the Channels for Reading PIPE and WriteStorage Pipe
	channel_storage_buffer := *user_input.line_storage_buffer
	//----------------------------Pipe Reader----------------------------------------------
	piped_lines := make(chan PipedMessage, channel_storage_buffer)

	start_pipe_reader(reader, piped_lines)

	//-----------------------------Writer Storage------------------------------------------
	// Make A slice of preallocated Array Buffers for the ThreadPool to write to.
	writer_storage := build_writer_storage(channel_storage_buffer)
	//-----------------------------Thread Pool---------------------------------------------
	// Make the pool and start processing piped lines
	thread_pool, pool_wait_group := thread.Make_pool(piped_lines, writer_storage)

	//-------------------------------Writer------------------------------------------
	draw_storage := make(chan WriterStorage,1)
	if *user_input.start_map {
		// Make A slice of preallocated Array Buffers for the ThreadPool to write to.
		draw_storage = build_writer_storage(channel_storage_buffer)
		go resources.Draw(draw_storage)
	}

	file_writer, opened_file := open_file(*user_input.output_path)
	uniqu_ips := make(map[string]bool)
	for {
		line, alive := <-writer_storage
		if !alive || line.Ip == LAST_MESSAGE_IDENTIFIER {
			break
		}
		_, ok := uniqu_ips[line.Ip]
		if ok {
			continue
		}
		uniqu_ips[line.Ip] = true
		if line.Longitude != 0 && line.Latitude != 0 {
			if *user_input.start_map {
				draw_storage <- line
			}
			write_to_file(&line,file_writer)
		}
	}
	_ = file_writer.Flush()
	close_file(opened_file)
	//--------------------------------------------------------------------------
	pool_wait_group.Wait()
	thread_pool.Release()

	fmt.Println("Finished")
}

/**
 * @Description: Create a buffered channel of user defined size
 * @param channel_buffer_size buffer size to associate to the channel
 * @return chan initialized channel
 */
func build_writer_storage(channel_buffer_size int) chan WriterStorage {
	var writer_storage = make(chan WriterStorage, channel_buffer_size)
	return writer_storage
}

/**
 * @Description: Open a file and return a writer and file object
 * @param output_path the path of the file we want to write to
 * @return *bufio.Writer File writer for open file
 * @return *os.File File object to be written too
 */
func open_file(output_path string) (*bufio.Writer, *os.File){
	const default_file_prermission = 0644
	opened_file, err := os.OpenFile(output_path, os.O_APPEND|os.O_WRONLY,
		default_file_prermission)
	if err != nil {
		color.Red(err.Error())
		return nil,nil
	}
	return bufio.NewWriter(opened_file) , opened_file
}

/**
 * @Description: Close and Open file
 * @param opened_file File object of the file which we wish to close
 */
func close_file(opened_file *os.File){
	err := opened_file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

/**
 * @Description: Format and write data to the file
 * @param line Struct of data to write to file
 * @param file_writer File writer to write with
 */
func write_to_file(line *WriterStorage, file_writer *bufio.Writer) {
	_, err := file_writer.WriteString(line.Ip + "," +
		line.Country + "," + line.RegionName + "," +
		line.City + "," + line.District + "," +
		line.Zip + "," + line.Isp + "," +
		line.Organization + "," + line.AssignedOrganization + "," +
		Bool_to_str(line.Proxy) + ", " +
		Bool_to_str(line.Mobile) + ", " +
		Bool_to_str(line.Hosting) + ", " +
		Float_to_str(line.Longitude) + "," +
		Float_to_str(line.Latitude) + "\n")
	if err != nil {
		color.Red(err.Error())
	}
}
