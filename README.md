# Procmap

Reads input from pipe looking for all valid IP addresses and converting them with semi-accurate geolocation(County level).
Each run will automatically print in a CSV file.

#### Features:

- Allow full process and multiple running pid monitoring.
- No network namespacing or sandboxing required
- Works with direct IP files or system tools (strace,fstrace,ptrace,netstat ...etc)
- Automatically parse line inputs for valid IPv4 Address
- Works in VM and Metal
- User customizable buffer size.
- No Lock based thread communication.
- Compatible with Windows, Mac and Linux

## How the UI looks

![strace](images/code-ui.png)

## How it Works

![strace](images/strace.png)

## Install

### Pre-built

Download and run the pre-built binary for your system from [Releases](https://gitlab.com/SparrowOchon/procmap/-/releases)

### From Source

#### Pre-requirement:

Built using Golang 1.13+ using Go Modules.

```
go build src/.
```

## Usage

```
 usage: procmap [-h|--help] [-i|--maxsize <integer>] [-o|--output "<value>"]

               Map PIPE'd IP addresses accurately on a map.
 Examples:      netstat -b | procmap -s <SAVE PATH>
               cat file.log | procmap
               echo <ip address> | procmap

Arguments:

  -h  --help     Print help information
  -i  --maxsize  Size of preallocated Write Buffer. Larger = Faster Writes
                 (Default: 100)
  -o  --output   Output Location (Defaults to PWD)

```

#### Examples on Linux:

Tracking by Process-Name on Linux

```
strace -f -e trace=network -v -s 65536 firefox | procmap -m
```

Tracking by Process-ids on Linux

```
strace -p 4813,90012,13444,12211 -f -e trace=network -v -s 65536 | procmap -m
```

Tracking by all Address using Netstat

```
netstat -antup | awk '{print $5}' | procmap -m
```

#### CSV Output:

All captures will be stored as in a CSV file which can be in a user defined path with the `-s` parameter or will be automatically generated

```
Country,Region,City,District,Zip,Isp,Organization,AssignedOrganization,Proxy,Mobile,Hosting,Long,Latitude
```

#### Why so much JS code ?:

Although there is more js files in this project they are all related to the earth in the map ui.
Which is a modified version of [Encom by Arscan](https://github.com/arscan/encom-globe). The codebase is a multipurpose scalable
golang pipe reader. Where the UI was used as a way to experiment with Golang UI libraries
due to the nature of wanting to use some form of open source GIS libs initially js was needed. Ultimately that was scrapped but
[Lorca](https://github.com/zserge/lorca) was kept due to the heavy changes already made to Encom's codebase to get that ui working.

Thus stripping the UI out will reduce this project and exec size to sub `3 mb` in size.
