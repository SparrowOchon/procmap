module gitlab.com/SparrowOchon/procmap

go 1.15

require (
	github.com/akamensky/argparse v1.2.2
	github.com/fatih/color v1.10.0
	github.com/panjf2000/ants v1.3.0
	github.com/zserge/lorca v0.1.9
	golang.org/x/sys v0.0.0-20200720211630-cb9d2d5c5666 // indirect
)
